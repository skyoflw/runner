Runner
===

require
---

0. .net 4.5

create project reason
---

### other software support

software|feature|filter|most top|drop item|show name|show description
:-|:-|:-:|:-:|:-:|:-:|:-:
DropIt|exe, bat, com, pif, cmd|yes|yes|yes|yes|no
XYplorer|any format|no|yes|no|yes|no
openxx|exe|yes|yes|no|yes|no

### what I want

0. support any format, like py or other script
0. support show description & name
0. support filter for single-file/single-folder/multi-file/multi-folder
0. most top for drop file or folder

more filter can code in script

okay, that is feature of the software.

how to use
---

0. download & run
0. click 'add' and select a script or software, or other file
0. select the file's support item (file or folder)
0. 'accept'
0. drop~

problem
---

0. config will create in software directory, any change will copy a backup config. the backup config need you manual delete. please sure the 'config.xml' file not the backup config!