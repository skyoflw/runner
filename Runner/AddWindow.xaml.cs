﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using Runner.RunnerObject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Runner
{
    /// <summary>
    /// AddWindow.xaml 的交互逻辑
    /// </summary>
    public partial class AddWindow : MetroWindow
    {
        private XRunner reusltXRunner;

        public AddWindow()
        {
            this.InitializeComponent();
        }

        private void ClickSelectButton(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            var result = ofd.ShowDialog();
            if (result == false) return;
            this.FileNameTextBox.Text = ofd.FileName;
        }

        public XRunner ReusltXRunner
        {
            get { return this.reusltXRunner; }
            set
            {
                this.reusltXRunner = value;

                if (value != null)
                {
                    this.FileNameTextBox.Text = value.Path;
                    this.InputHintTextBox.Text = value.Hint;
                    this.InputNameTextBox.Text = value.Name;
                    this.FirstParamterTextBox.Text = value.Paramter;

                    if (value.Support.HasFlag(FileSystemTestType.File))
                        this.SupportFileCheckBox.IsChecked = true;

                    if (value.Support.HasFlag(FileSystemTestType.Directory))
                        this.SupportFolderCheckBox.IsChecked = true;

                    if (value.Support.HasFlag(FileSystemTestType.MultipleFile))
                        this.SupportMultipleFileCheckBox.IsChecked = true;

                    if (value.Support.HasFlag(FileSystemTestType.MultipleDirectory))
                        this.SupportMultipleFolderCheckBox.IsChecked = true;
                }
            }
        }

        public async void Accept()
        {
            if (this.SupportFileCheckBox.IsChecked != true && this.SupportFolderCheckBox.IsChecked != true && this.SupportMultipleFileCheckBox.IsChecked != true && this.SupportMultipleFolderCheckBox.IsChecked != true)
            {
                await this.ShowMessageAsync("error", "not support any item");
                return;
            }

            if (this.reusltXRunner == null)
                this.reusltXRunner = new XRunner();

            this.reusltXRunner.Support = FileSystemTestType.None;

            if (this.SupportFileCheckBox.IsChecked == true)
                this.reusltXRunner.Support |= FileSystemTestType.File;

            if (this.SupportFolderCheckBox.IsChecked == true)
                this.reusltXRunner.Support |= FileSystemTestType.Directory;

            if (this.SupportMultipleFileCheckBox.IsChecked == true)
                this.reusltXRunner.Support |= FileSystemTestType.MultipleFile;

            if (this.SupportMultipleFolderCheckBox.IsChecked == true)
                this.reusltXRunner.Support |= FileSystemTestType.MultipleDirectory;

            this.reusltXRunner.Path = this.FileNameTextBox.Text;
            this.reusltXRunner.Paramter = this.FirstParamterTextBox.Text;
            this.reusltXRunner.Hint = this.InputHintTextBox.Text;
            this.reusltXRunner.Name = this.InputNameTextBox.Text.Trim();

            if (this.reusltXRunner.Name.Length == 0)
                this.reusltXRunner.Name = System.IO.Path.GetFileNameWithoutExtension(this.reusltXRunner.Path);
            if (this.reusltXRunner.Name.Length == 0)
                this.reusltXRunner.Name = System.IO.Path.GetFileName(this.reusltXRunner.Path);

            this.DialogResult = true;
        }

        private void ClickAcceptButton(object sender, RoutedEventArgs e)
        {
            this.Accept();
        }
    }
}
