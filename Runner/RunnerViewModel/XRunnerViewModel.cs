﻿using Runner.RunnerObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Runner.RunnerViewModel
{
    public class XRunnerViewModel : JasilyViewModel<XRunner>
    {
        public XRunnerViewModel(XRunner runner)
            : base(runner)
        {
        }

        public string FileName
        {
            get { return Path.GetFileName(this.Source.Path); }
        }

        public string Hint
        {
            get { return this.Source.Hint ?? this.Source.Path; }
        }

        public string DisplayMode
        {
            get
            {
                var l = new List<string>();
                if (this.Source.Support.HasFlag(FileSystemTestType.File))
                    l.Add("file");
                if (this.Source.Support.HasFlag(FileSystemTestType.Directory))
                    l.Add("folder");
                if (this.Source.Support.HasFlag(FileSystemTestType.MultipleFile))
                    l.Add("multi-file");
                if (this.Source.Support.HasFlag(FileSystemTestType.MultipleDirectory))
                    l.Add("multi-folder");
                return String.Join(" | ", l);
            }
        }

        public void Run(IEnumerable<string> paths)
        {
            if (!File.Exists(this.Source.Path))
                return;

            var paras = paths.ToList();

            if (paras.Count > 0 && this.Source.Support.IsMatch(paras))
            {
                if (!String.IsNullOrWhiteSpace(this.Source.Paramter))
                    paras.Insert(0, this.Source.Paramter);

                var arguments = String.Join(" ", paras.Select(z => "\"" + z + "\""));
                Process.Start(this.Source.Path, arguments);
            }
        }
    }
}
