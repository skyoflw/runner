﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runner.RunnerObject
{
    public class XRunner
    {
        public FileSystemTestType Support { get; set; }

        public string Path { get; set; }

        public string Paramter { get; set; }

        public string Name { get; set; }

        public string Hint { get; set; }
    }
}
