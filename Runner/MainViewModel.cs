﻿using Runner.RunnerObject;
using Runner.RunnerViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Runner
{
    public class MainViewModel
    {
        private const string RunnerDataDirectory = "AppData.Runner";
        private const string Config = RunnerDataDirectory + "\\config.xml";

        bool IsChanged;

        public MainViewModel()
        {
            this.Items = new ObservableCollection<XRunnerViewModel>();
        }

        public ObservableCollection<XRunnerViewModel> Items { get; private set; }

        public void Load()
        {
            if (File.Exists(Config))
            {
                var serializer = new XmlSerializer(typeof(List<XRunner>));
                using (var stream = File.OpenRead(Config))
                    foreach (var item in serializer.Deserialize(stream) as List<XRunner>)
                        this.Items.Add(new XRunnerViewModel(item));
            }
        }

        public void AddXRunner(XRunner runner)
        {
            this.Items.Add(new XRunnerViewModel(runner));
            this.IsChanged = true;
        }

        public void Delete(XRunnerViewModel vm)
        {
            this.Items.Remove(vm);
            this.IsChanged = true;
        }

        public void Save()
        {
            if (!this.IsChanged) return;

            if (!Directory.Exists(RunnerDataDirectory))
                Directory.CreateDirectory(RunnerDataDirectory);

            if (File.Exists(Config))
                File.Copy(Config, RunnerDataDirectory + "\\config." + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".xml");

            var serializer = new XmlSerializer(typeof(List<XRunner>));
            using (var stream = File.Open(Config, FileMode.Create, FileAccess.Write, FileShare.Write))
                serializer.Serialize(stream, this.Items.Select(z => z.Source).OrderBy(z => z.Name).ToList());
        }
    }
}
