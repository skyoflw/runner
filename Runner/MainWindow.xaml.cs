﻿using MahApps.Metro.Controls;
using Runner.Properties;
using Runner.RunnerViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Runner
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        MainViewModel ViewModel;

        public MainWindow()
        {
            this.InitializeComponent();

            double h = SystemParameters.PrimaryScreenHeight;
            double w = SystemParameters.PrimaryScreenWidth;

            this.Left = w - this.Width - 40;
            this.Top = h - this.Height - 80;
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            this.Height = Settings.Default.MainWindowHeight;

            this.ViewModel = new MainViewModel();
            this.DataContext = this.ViewModel;
            this.ViewModel.Load();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            Settings.Default.MainWindowHeight = this.Height;
            Settings.Default.Save();

            try
            {
                this.ViewModel.Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show("无法保存配置：" + ex.ToString());
                e.Cancel = true;
            }
        }

        private void ClickAddButton(object sender, RoutedEventArgs e)
        {
            var w = new AddWindow();
            this.Topmost = false;
            if (w.ShowDialog() == true)
            {
                this.ViewModel.AddXRunner(w.ReusltXRunner);
            }
            this.Topmost = true;
        }

        private void ClickDeleteButton(object sender, RoutedEventArgs e)
        {
            var frameworkElement = sender as FrameworkElement;
            if (frameworkElement != null)
                this.ViewModel.Delete(frameworkElement.DataContext as XRunnerViewModel);
        }

        private void Grid_DragEnter(object sender, DragEventArgs e)
        {
            var frameworkElement = sender as FrameworkElement;
            if (frameworkElement != null)
                this.ItemsListView.SelectedItem = frameworkElement.DataContext;
        }

        private void Grid_Drop(object sender, DragEventArgs e)
        {
            var frameworkElement = sender as FrameworkElement;
            if (frameworkElement != null)
            {
                var vm = frameworkElement.DataContext as XRunnerViewModel;
            
                if (vm != null)
                {
                    if (e.Data.GetDataPresent(DataFormats.FileDrop))
                    {
                        string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                        vm.Run(files);
                    }
                }
            }
        }

        private void ClickEditButton(object sender, RoutedEventArgs e)
        {
            var vm = (sender as FrameworkElement).DataContext as XRunnerViewModel;

            var w = new AddWindow();
            w.ReusltXRunner = vm.Source;
            this.Topmost = false;
            if (w.ShowDialog() == true)
            {
                this.ViewModel.Delete(vm);
                this.ViewModel.AddXRunner(w.ReusltXRunner);
            }
            this.Topmost = true;
        }
    }
}
